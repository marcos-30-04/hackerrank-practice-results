"use strict";

const { SSL_OP_SSLEAY_080_CLIENT_DH_BUG } = require("constants");
const fs = require("fs");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", function (inputStdin) {
  inputString += inputStdin;
});

process.stdin.on("end", function () {
  inputString = inputString.split("\n");

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'caesarCipher' function below.
 *
 * The function is expected to return a STRING.
 * The function accepts following parameters:
 *  1. STRING s
 *  2. INTEGER k
 */

function caesarCipher(s, k) {
  // Write your code here
  while (k > 25) k = k - 26;
  let alphabet = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "h",
    "i",
    "j",
    "k",
    "l",
    "m",
    "n",
    "o",
    "p",
    "q",
    "r",
    "s",
    "t",
    "u",
    "v",
    "w",
    "x",
    "y",
    "z",
  ];

  let removed = alphabet.slice(0, k);
  let shifted = alphabet.slice(k);
  shifted.push(...removed);

  let string = s.split("");

  for (let i = 0; i < string.length; i++) {
    if (!/[a-z]/i.test(string[i])) continue;

    let index = alphabet.indexOf(string[i].toLowerCase());

    if (string[i] == string[i].toUpperCase()) {
      string[i] = shifted[index].toUpperCase();
    } else string[i] = shifted[index];
  }

  string = string.join("");
  return string;
}

function main() {
  const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

  const n = parseInt(readLine().trim(), 10);

  const s = readLine();

  const k = parseInt(readLine().trim(), 10);

  const result = caesarCipher(s, k);

  ws.write(result + "\n");

  ws.end();
}
console.log(caesarCipher("jasjhdoqer-qweqweewqaaaaa", 87));
