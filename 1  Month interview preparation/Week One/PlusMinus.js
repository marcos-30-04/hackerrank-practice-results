"use strict";

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", function (inputStdin) {
  inputString += inputStdin;
});

process.stdin.on("end", function () {
  inputString = inputString.split("\n");

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'plusMinus' function below.
 *
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

function plusMinus(arr) {
  let length = arr.length;

  let positive = 0.0;
  let negative = 0.0;
  let zero = 0.0;

  for (let i = 0; i < length; i++) {
    if (arr[i] === 0) zero++;
    if (arr[i] > 0) positive++;
    if (arr[i] < 0) negative++;
  }

  positive = (positive / length).toFixed(6);
  negative = (negative / length).toFixed(6);
  zero = (zero / length).toFixed(6);

  console.log(positive);
  console.log(negative);
  console.log(zero);
}

function main() {
  const n = parseInt(readLine().trim(), 10);

  const arr = readLine()
    .replace(/\s+$/g, "")
    .split(" ")
    .map((arrTemp) => parseInt(arrTemp, 10));

  plusMinus(arr);
}
