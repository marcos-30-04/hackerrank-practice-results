"use strict";

const fs = require("fs");

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", function (inputStdin) {
  inputString += inputStdin;
});

process.stdin.on("end", function () {
  inputString = inputString.split("\n");

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'flippingBits' function below.
 *
 * The function is expected to return a LONG_INTEGER.
 * The function accepts LONG_INTEGER n as parameter.
 */

function flippingBits(n) {
  // Write your code here

  let binary32Bit = n.toString(2);
  binary32Bit = binary32Bit.padStart(32, "0");
  console.log(binary32Bit + " to binary");

  let bits = { 0: "1", 1: "0" };
  let flipped32Bit = binary32Bit.replace(/[01]/g, (n) => bits[n]);
  console.log(flipped32Bit + " flipped");

  let result = parseInt(flipped32Bit, 2);
  console.log(result + " result");
  return result;
}

function main() {
  const ws = fs.createWriteStream(process.env.OUTPUT_PATH);

  const q = parseInt(readLine().trim(), 10);

  for (let qItr = 0; qItr < q; qItr++) {
    const n = parseInt(readLine().trim(), 10);

    const result = flippingBits(n);

    ws.write(result + "\n");
  }

  ws.end();
}
