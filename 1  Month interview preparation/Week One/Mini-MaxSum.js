"use strict";

process.stdin.resume();
process.stdin.setEncoding("utf-8");

let inputString = "";
let currentLine = 0;

process.stdin.on("data", function (inputStdin) {
  inputString += inputStdin;
});

process.stdin.on("end", function () {
  inputString = inputString.split("\n");

  main();
});

function readLine() {
  return inputString[currentLine++];
}

/*
 * Complete the 'miniMaxSum' function below.
 *
 * The function accepts INTEGER_ARRAY arr as parameter.
 */

function miniMaxSum(arr) {
  // Write your code here
  const length = arr.length;
  const sorted = arr.sort((a, b) => a - b);

  let min = sorted.slice(0, length - 1);
  let max = sorted.slice(1, length);

  max = max.reduce((acc, current) => {
    return acc + current;
  });

  min = min.reduce((acc, current) => {
    return acc + current;
  });

  console.log(`${min} ${max}`);
}

function main() {
  const arr = readLine()
    .replace(/\s+$/g, "")
    .split(" ")
    .map((arrTemp) => parseInt(arrTemp, 10));

  miniMaxSum(arr);
}

miniMaxSum([8, 19, 6845, 987, 5]);
